package com.sai.learnsheetnote

import androidx.test.core.app.ApplicationProvider
import com.sai.learnsheetnote.models.Pitch
import org.junit.Test
import com.google.common.truth.Truth.assertWithMessage
import com.sai.learnsheetnote.models.AllPitches
import com.sai.learnsheetnote.models.Note
import com.sai.learnsheetnote.models.nextPitch
import kotlin.random.Random
import android.content.Context

class PitchTest {
    val context = ApplicationProvider.getApplicationContext<Context>()

    //    @Test
//    fun pitchInitJsonTest() {
//        val pitch = Json.decodeFromString<Pitch>(jsonPitch)
//        assertWithMessage("Pitch is null").that(pitch).isNotNull()
//        assertWithMessage("Pitch octave is correct").that(pitch.octave).isEqualTo(-1)
//        assertWithMessage("Pitch freq is correct").that(pitch.frequency).isEqualTo(8.1758)
//    }
//
//    companion object {
//        const val jsonPitch = """
//{"id":"0", "name":"c", "octave": -1, "freq":8.1758}"""
//    }
    @Test
    fun pitchNameTest() {
        val pitch = Pitch(72)
        assertWithMessage("Pitch Name Problem").that(pitch.name).isEqualTo("C")

    }

    @Test
    fun pitchOctaveTest() {
        val pitch = Pitch(72)
        assertWithMessage("Pitch Octave Problem").that(pitch.octave).isEqualTo(5)
    }

    @Test
    fun pitchThrowPitchTest() {
        try {
            val pitch = Pitch(-5)
            assertWithMessage("Pitch Null Problem").that(pitch).isNotNull()
        } catch (exp: Exception) {
            println(exp.message)
        }

    }

    @Test
    fun getRandomPitchTest() {
        for (i in 0..20) {
            val randP = Random.nextPitch(0, 2)
            println("id :${randP.id} in Ocatve: ${randP.octave}, name: ${randP.name}")
            assertWithMessage("random pitch not selected {${randP.name}}").that(randP)
                .isNotNull()
        }
    }

//    @Test
//    fun getImageTest() {
//
//        val randNote = Note(Random.nextPitch(2, 6), 24, context)
//        println("in Ocatve: ${randNote.octave}, name: ${randNote.pitch.name}")
//        val imageViews = randNote.getImageViews()
//        assertWithMessage("number of message is incorrect").that(imageViews.size).isEqualTo(2)
//    }
}