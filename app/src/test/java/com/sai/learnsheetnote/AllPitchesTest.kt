package com.sai.learnsheetnote

import com.sai.learnsheetnote.models.AllPitches
import org.junit.Test
import com.google.common.truth.Truth.assertWithMessage

class AllPitchesTest {

    init {
        AllPitches.makeAllPitchses(samplePitches)
    }

    @Test
    fun initAllPitches() {
        assertWithMessage("All Pitches is Null").that(AllPitches.allPitches.size).isGreaterThan(0)
        val currentPitch = AllPitches.get(8)
        assertWithMessage("pitch is not created").that(currentPitch).isNotNull()
        assertWithMessage("pitch is in not correct position").that(currentPitch.id).isEqualTo(8)

    }

    @Test
    fun getRandomPitchTest() {
        for (i in 0..20) {
            val randP = AllPitches.getRandomPitch()
            print(randP.name)
            assertWithMessage("random pitch not selected {${randP.name}}").that(randP).isNotNull()
        }
    }

    @Test
    fun findRespectivePitchOfFrequencyTest() {
        val pitch = AllPitches.findRespectivePitchOfFrequency(1540.0)
        assertWithMessage("found pitch is not correct").that(pitch).isNotNull()
        assertWithMessage("found pitch name is not correct").that(pitch.name).isEqualTo("G")
        assertWithMessage("found pitch Id is not correct").that(pitch.id).isEqualTo(12)

    }

    companion object {
        val samplePitches = """
           {"error": 0,
"pitches": [
{"id":"0", "name":"c", "octave": -1, "freq":8.1758},
{"id":"1", "name":"C♯/D♭", "octave":-1, "freq":8.6620},
{"id":"2", "name":"d", "octave":-1, "freq":9.1770},
{"id":"3", "name":"E♭/D♯", "octave": -1, "freq": 9.7227},
{"id":"4", "name":"E", "octave": -1, "freq": 10.301 },
{"id":"5", "name":"F", "octave": -1, "freq": 10.914 },
{"id":"6", "name":"F♯/G♭", "octave": -1, "freq": 11.563 },
{"id":"7", "name":"G", "octave": -1, "freq": 12.250 },
{"id":"8", "name":"A♭/G♯", "octave": -1, "freq": 12.979 },
{"id":"9", "name":"A", "octave": -1, "freq": 13.750 },
{"id":"10", "name":"B♭/A♯", "octave": -1, "freq": 14.568 },
{"id":"11", "name":"B", "octave": -1, "freq": 15.434 },

{"id":"12", "name":"c", "octave":0, "freq":16.352},
{"id":"13", "name":"C♯/D♭", "octave":0, "freq":17.324},
{"id":"14", "name":"d", "octave":0, "freq":18.354},
{"id":"15", "name":"E♭/D♯", "octave": 0, "freq": 19.445 },
{"id":"16", "name":"E", "octave": 0, "freq": 20.602 },
{"id":"17", "name":"F", "octave": 0, "freq": 21.827 },
{"id":"18", "name":"F♯/G♭", "octave": 0, "freq": 23.125 },
{"id":"19", "name":"G", "octave": 0, "freq": 24.500 },
{"id":"20", "name":"A♭/G♯", "octave": 0, "freq": 25.957  },
{"id":"21", "name":"A", "octave": 0, "freq":  27.500 },
{"id":"22", "name":"B♭/A♯", "octave": 0, "freq":   29.135},
{"id":"23", "name":"B", "octave": 0, "freq":  30.868 },

{"id":"24", "name":"c", "octave":1, "freq":32.703},
{"id":"25", "name":"C♯/D♭", "octave":1, "freq":34.648},
{"id":"26", "name":"d", "octave":1, "freq":36.708},
{"id":"27", "name":"E♭/D♯", "octave": 1, "freq": 38.891 },
{"id":"28", "name":"E", "octave": 1, "freq": 41.203 },
{"id":"29", "name":"F", "octave": 1, "freq": 43.654 },
{"id":"30", "name":"F♯/G♭", "octave": 1, "freq": 46.249 },
{"id":"31", "name":"G", "octave": 1, "freq": 48.999 },
{"id":"32", "name":"A♭/G♯", "octave": 1, "freq": 51.913},
{"id":"33", "name":"A", "octave": 1, "freq":  55.000 },
{"id":"34", "name":"B♭/A♯", "octave": 1, "freq": 58.270},
{"id":"35", "name":"B", "octave": 1, "freq": 61.735},

{"id":"36", "name":"c", "octave":2, "freq":65.406},
{"id":"37", "name":"C♯/D♭", "octave":2, "freq":69.296},
{"id":"38", "name":"d", "octave":2, "freq":73.416},
{"id":"39", "name":"E♭/D♯", "octave": 2, "freq": 77.782 },
{"id":"40", "name":"E", "octave": 2, "freq": 82.407 },
{"id":"41", "name":"F", "octave": 2, "freq": 87.307 },
{"id":"42", "name":"F♯/G♭", "octave": 2, "freq": 92.499 },
{"id":"43", "name":"G", "octave": 2, "freq": 97.999 },
{"id":"44", "name":"A♭/G♯", "octave": 2, "freq": 103.83  },
{"id":"45", "name":"A", "octave": 2, "freq": 110.00},
{"id":"46", "name":"B♭/A♯", "octave": 2, "freq": 116.54 },
{"id":"47", "name":"B", "octave": 2, "freq":  123.47 },

{"id":"48", "name":"c", "octave":3, "freq":130.81},
{"id":"49", "name":"C♯/D♭", "octave":3, "freq":138.59},
{"id":"50", "name":"d", "octave":3, "freq":146.83},
{"id":"51", "name":"E♭/D♯", "octave": 3, "freq": 155.56 },
{"id":"52", "name":"E", "octave": 3, "freq": 164.81 },
{"id":"53", "name":"F", "octave": 3, "freq": 174.61 },
{"id":"54", "name":"F♯/G♭", "octave": 3, "freq": 185.00 },
{"id":"55", "name":"G", "octave": 3, "freq": 196.00 },
{"id":"56", "name":"A♭/G♯", "octave": 3, "freq": 207.65 },
{"id":"57", "name":"A", "octave": 3, "freq": 220.00  },
{"id":"58", "name":"B♭/A♯", "octave": 3, "freq": 233.08 },
{"id":"59", "name":"B", "octave": 3, "freq":  246.94},

{"id":"60", "name":"c", "octave":4, "freq":261.63},
{"id":"61", "name":"C♯/D♭", "octave":4, "freq":277.18},
{"id":"62", "name":"d", "octave":4, "freq":293.66},
{"id":"63", "name":"E♭/D♯", "octave": 4, "freq": 311.13 },
{"id":"64", "name":"E", "octave": 4, "freq": 329.63 },
{"id":"65", "name":"F", "octave": 4, "freq": 349.23 },
{"id":"66", "name":"F♯/G♭", "octave": 4, "freq": 369.99 },
{"id":"67", "name":"G", "octave": 4, "freq": 392.00 },
{"id":"68", "name":"A♭/G♯", "octave": 4, "freq": 415.30 },
{"id":"69", "name":"A", "octave": 4, "freq": 440.00},
{"id":"70", "name":"B♭/A♯", "octave": 4, "freq":  466.16},
{"id":"71", "name":"B", "octave": 4, "freq": 493.88 },

{"id":"72", "name":"c", "octave":5, "freq":523.25},
{"id":"73", "name":"C♯/D♭", "octave":5, "freq":554.37},
{"id":"74", "name":"d", "octave":5, "freq":587.33},
{"id":"75", "name":"E♭/D♯", "octave": 5, "freq": 622.25 },
{"id":"76", "name":"E", "octave": 5, "freq": 659.26 },
{"id":"77", "name":"F", "octave": 5, "freq": 698.46 },
{"id":"78", "name":"F♯/G♭", "octave": 5, "freq": 739.99},
{"id":"79", "name":"G", "octave": 5, "freq": 783.99},
{"id":"80", "name":"A♭/G♯", "octave": 5, "freq": 830.61 },
{"id":"81", "name":"A", "octave": 5, "freq":  880.00},
{"id":"82", "name":"B♭/A♯", "octave": 5, "freq":  932.33},
{"id":"83", "name":"B", "octave": 5, "freq": 987.77 },

{"id":"84", "name":"c", "octave":6, "freq":1046.5},
{"id":"85", "name":"C♯/D♭", "octave":6, "freq":1108.7},
{"id":"86", "name":"d", "octave":6, "freq":1174.7},
{"id":"87", "name":"E♭/D♯", "octave": 6, "freq": 1244.5 },
{"id":"88", "name":"E", "octave": 6, "freq": 1318.5 },
{"id":"89", "name":"F", "octave": 6, "freq": 1396.9 },
{"id":"90", "name":"F♯/G♭", "octave": 6, "freq": 1480.0 },
{"id":"91", "name":"G", "octave": 6, "freq": 1568.0 },
{"id":"92", "name":"A♭/G♯", "octave": 6, "freq": 1661.2 },
{"id":"93", "name":"A", "octave": 6, "freq": 1760.0 },
{"id":"94", "name":"B♭/A♯", "octave": 6, "freq": 1864.7 },
{"id":"95", "name":"B", "octave": 6, "freq": 1975.5 },

{"id":"96", "name":"c", "octave":7, "freq":2093.0},
{"id":"97", "name":"C♯/D♭", "octave":7, "freq":2217.5},
{"id":"98", "name":"d", "octave":7, "freq":2349.3},
{"id":"99", "name":"E♭/D♯", "octave": 7, "freq": 2489.0 },
{"id":"100", "name":"E", "octave": 7, "freq": 2637.0 },
{"id":"101", "name":"F", "octave": 7, "freq": 2793.8 },
{"id":"102", "name":"F♯/G♭", "octave": 7, "freq": 2960.0 },
{"id":"103", "name":"G", "octave": 7, "freq": 3136.0 },
{"id":"104", "name":"A♭/G♯", "octave": 7, "freq": 3322.4},
{"id":"105", "name":"A", "octave": 7, "freq": 3520.0 },
{"id":"106", "name":"B♭/A♯", "octave": 7, "freq": 3729.3},
{"id":"107", "name":"B", "octave": 7, "freq": 3951.1},

{"id":"108", "name":"c", "octave":8, "freq":4186.0},
{"id":"109", "name":"C♯/D♭", "octave":8, "freq":4434.9},
{"id":"110", "name":"d", "octave":8, "freq":4698.6},
{"id":"111", "name":"E♭/D♯", "octave": 8, "freq": 4978.0 },
{"id":"112", "name":"E", "octave": 8, "freq": 5274.0 },
{"id":"113", "name":"F", "octave": 8, "freq": 5587.7 },
{"id":"114", "name":"F♯/G♭", "octave": 8, "freq": 5919.9 },
{"id":"115", "name":"G", "octave": 8, "freq": 6271.9 },
{"id":"116", "name":"A♭/G♯", "octave": 8, "freq": 6644.9 },
{"id":"117", "name":"A", "octave": 8, "freq": 7040.0},
{"id":"118", "name":"B♭/A♯", "octave": 8, "freq": 7458.6 },
{"id":"119", "name":"B", "octave": 8, "freq":  7902.1},

{"id":"120", "name":"c", "octave":9, "freq":8372.0},
{"id":"121", "name":"C♯/D♭", "octave":9, "freq":8869.8},
{"id":"122", "name":"d", "octave":9, "freq":9397.3},
{"id":"123", "name":"E♭/D♯", "octave": 9, "freq": 9956.1 },
{"id":"124", "name":"E", "octave": 9, "freq": 10548 },
{"id":"125", "name":"F", "octave": 9, "freq": 11175 },
{"id":"126", "name":"F♯/G♭", "octave": 9, "freq": 11840 },
{"id":"127", "name":"G", "octave": 9, "freq": 12544 },
{"id":"128", "name":"A♭/G♯", "octave": 9, "freq": 13290 },
{"id":"129", "name":"A", "octave": 9, "freq": 14080 },
{"id":"130", "name":"B♭/A♯", "octave": 9, "freq": 14917},
{"id":"131", "name":"B", "octave": 9, "freq": 15804},

{"id":"132", "name":"c", "octave":10, "freq":16744},
{"id":"133", "name":"C♯/D♭", "octave":10, "freq":17740},
{"id":"134", "name":"d", "octave":10, "freq":18795},
{"id":"135", "name":"E♭/D♯", "octave": 10, "freq": 19912 },
{"id":"136", "name":"E", "octave": 10, "freq": 21096 },
{"id":"137", "name":"F", "octave": 10, "freq": 22351 },
{"id":"138", "name":"F♯/G♭", "octave": 10, "freq": 23680 },
{"id":"139", "name":"G", "octave": 10, "freq": 25088 },
{"id":"140", "name":"A♭/G♯", "octave": 10, "freq": 26580 },
{"id":"141", "name":"A", "octave": 10, "freq":  28160},
{"id":"142", "name":"B♭/A♯", "octave": 10, "freq": 29834  },
{"id":"143", "name":"B", "octave": 10, "freq": 31609 }

]}
"""
    }
}