package com.sai.learnsheetnote.models

import be.tarsos.dsp.util.PitchConverter
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.json.JSONObject
import java.util.*


object AllPitches {

    lateinit var allPitches: Array<Pitch>

    fun makeAllPitchses(allPitchesString: String) {
        val jsonObject = JSONObject(allPitchesString)
        val allPitchesJsonArray = jsonObject.getJSONArray("pitches")
        allPitches = Array(allPitchesJsonArray.length()) { i ->
            Json.decodeFromString(allPitchesJsonArray.getJSONObject(i).toString())
        }
    }

    fun get(i: Int): Pitch {
        return allPitches[i]
    }

    fun getRandomPitch(): Pitch {
        return allPitches[Random().nextInt(allPitches.size)]
    }

    fun findRespectivePitchOfFrequency(freq: Double): Pitch {
        return get(PitchConverter.hertzToMidiKey(freq))
    }
//    constructor(allPitchesString: String) : this() {
//
//        allPitches = Array(allPitchesJsonArray.length()) {
//
//        }

//    }

//    val allPitches: Array<Pitch>

//    init {
////        val pitchesStr: String = UF.readFile(R.raw.pitches, getApplicationContext())
//        val `is`: InputStream = .getResources().openRawResource(id)
//        val pitchesStr = File(R.raw.pitches)
//        allPitches = Array()
//    }

}