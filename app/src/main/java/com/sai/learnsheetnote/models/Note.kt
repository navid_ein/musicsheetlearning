package com.sai.learnsheetnote.models

import android.content.Context
import android.widget.ImageView
import com.sai.learnsheetnote.App
import com.sai.learnsheetnote.R
import com.sai.learnsheetnote.models.NoteProperties.Companion.NumberOfOctave
import com.sai.learnsheetnote.models.NoteProperties.Companion.PitchNumber
import com.sai.learnsheetnote.models.NoteProperties.NoteNames


class Note(private val pitch: Pitch) {

    val position: Int by lazy {
        var pos = -1
        when (pitch.id % PitchNumber) {
            NoteNames.C -> pos = 0
            NoteNames.Cs -> pos = 0
            NoteNames.D -> pos = 1
            NoteNames.Ds -> pos = 1
            NoteNames.E -> pos = 2
            NoteNames.F -> pos = 3
            NoteNames.Fs -> pos = 3
            NoteNames.G -> pos = 4
            NoteNames.Gs -> pos = 4
            NoteNames.A -> pos = 5
            NoteNames.As -> pos = 5
            NoteNames.B -> pos = 6
            else -> -1
        }
        pos += octave * NumberOfOctave
        (11 - pos)
    }

    val isSharp: Boolean by lazy {
        when (pitch.id % PitchNumber) {
            NoteNames.C -> false
            NoteNames.Cs -> true
            NoteNames.D -> false
            NoteNames.Ds -> true
            NoteNames.E -> false
            NoteNames.F -> false
            NoteNames.Fs -> true
            NoteNames.G -> false
            NoteNames.Gs -> true
            NoteNames.A -> false
            NoteNames.As -> true
            NoteNames.B -> false
            else -> false
        }
    }
    val name: String by lazy {
        pitch.name
    }

    val midiId: Int by lazy {
        pitch.id
    }

    val octave: Int by lazy {
        var octave = pitch.id / PitchNumber
        octave -= 5
        if (octave > 2) {
            octave = 2
        } else if (octave < -2) {
            octave = -2
        }
        octave
    }

    fun isTop(): Boolean {
        return octave > 4
    }

//    val noteHeight: Int by lazy {
//        xHeight * 16
//    }
//    val noteWidth: Int by lazy {
//        xHeight * 4
//    }

//    fun getImageViews(): Array<ImageView> {
//
//        val noteImageViews = ArrayList<ImageView>()
//        val noteImage = ImageView(App.context)
//        noteImage.layoutParams.height = xHeight * 16
//        noteImage.layoutParams.width = xHeight * 4
//        noteImage.requestLayout()
//        if (octave > 4) {
//            noteImage.setImageResource(R.drawable.main_note_top)
//        } else {
//            noteImage.setImageResource(R.drawable.main_note_bottom)
//        }
//        noteImageViews.add(noteImage)
//
//
//        if (isSharp) {
//            val sharpImage = ImageView(App.context)
//            sharpImage.layoutParams.height = xHeight * 16
//            sharpImage.layoutParams.width = xHeight * 4
//            sharpImage.requestLayout()
//            sharpImage.setImageResource(R.drawable.sharp)
//            noteImageViews.add(sharpImage)
//        }
//
//        return noteImageViews.toTypedArray()
//
//    }


}