package com.sai.learnsheetnote.models

import com.sai.learnsheetnote.models.NoteProperties.Companion.PitchNumber
import com.sai.learnsheetnote.models.NoteProperties.NoteNames

import kotlinx.serialization.Serializable
import java.lang.Exception
import kotlin.random.Random


//fun Random.nextPitch(fromNote: Int, toNote: Int): Pitch {
//    val bound = toNote - fromNote
//    val random = Random.nextInt(bound) + fromNote
//    return Pitch(random)
//}

fun Random.nextPitch(fromOctave: Int, toOctave: Int): Pitch {
    val bound = (toOctave - fromOctave) * PitchNumber
    println(bound)
    val random = Random.nextInt(bound) + (fromOctave * PitchNumber)
    return Pitch(random)
}

@Serializable
class Pitch(
    val id: Int
) {

    init {
        if (id < 0) throw Exception("id must be greater than 0")
    }

    val name: String by lazy {
        when (id % PitchNumber) {
            NoteNames.C -> "C"
            NoteNames.Cs -> "C♯/D♭"
            NoteNames.D -> "D"
            NoteNames.Ds -> "E♭/D♯"
            NoteNames.E -> "E"
            NoteNames.F -> "F"
            NoteNames.Fs -> "F♯/G♭"
            NoteNames.G -> "G"
            NoteNames.Gs -> "A♭/G♯"
            NoteNames.A -> "A"
            NoteNames.As -> "B♭/A♯"
            NoteNames.B -> "B"
            else -> ""
        }
    }

    val octave: Int by lazy {
        (id / PitchNumber) - 1
    }
}

class NoteProperties {
    class NoteNames {
        companion object {
            const val C = 0
            const val Cs = 1
            const val D = 2
            const val Ds = 3
            const val E = 4
            const val F = 5
            const val Fs = 6
            const val G = 7
            const val Gs = 8
            const val A = 9
            const val As = 10
            const val B = 11
        }
    }

    companion object {

        const val PitchNumber = 12
        const val NumberOfOctave = 7
    }
}