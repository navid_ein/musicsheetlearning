package com.sai.learnsheetnote.models

import io.reactivex.subjects.PublishSubject
import kotlin.collections.ArrayList
import kotlin.random.Random

class Sheet(override var notesOnSheet: Int, override var noteHeight: Int) :
    com.navid.sheetmusic.abstracts.ISheet {

    companion object {
        const val fromOctave = 4
        const val toOctave = 7
    }

//    var randomNoteGenerator: com.navid.sheetmusic.abstracts.IRandomNoteSelector? = null


    var randomNotePublisher = PublishSubject.create<Note?>()

    val randomNotes: ArrayList<Note> = ArrayList()
    var userNote: Note? = null

    fun selectRandomNote() {
        val randomNote = Note(Random.nextPitch(fromOctave, toOctave))
        println("LSN: selected Note is ${randomNote?.octave}: ${randomNote?.midiId} = ${randomNote?.name}")
        randomNotePublisher.onNext(randomNote!!)
        randomNotes.add(randomNote)
    }

    fun getRandomNoteCount(): Int {
        return randomNotes.size
    }

    fun getRandomNote(index: Int): Note {
        return randomNotes[index]

    }

    override fun getNotePosition(id: Int) {
        TODO("Not yet implemented")
    }

    override fun getKeyImage() {
        TODO("Not yet implemented")
    }


}