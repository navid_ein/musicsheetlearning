package com.sai.learnsheetnote.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.sai.learnsheetnote.R
import com.sai.learnsheetnote.models.Note
import com.sai.learnsheetnote.models.Sheet
import kotlinx.android.synthetic.main.note_item.view.*


fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

class RandomNoteAdapter(private val sheet: Sheet, private val baseHeight: Int) :
    RecyclerView.Adapter<RandomNoteAdapter.NoteHolder>() {

    class NoteHolder(itemView: View, private val baseHeight: Int) :
        RecyclerView.ViewHolder(itemView) {
        private val view: View = itemView
        private val height = baseHeight * 16
        private val width = baseHeight * 4
        fun bindView(note: Note) {
            // linear layout
            val marginParams = MarginLayoutParams(view.randomNoteLL.layoutParams)
            marginParams.setMargins(
                marginParams.leftMargin,
                note.position * baseHeight,
                marginParams.rightMargin,
                marginParams.bottomMargin
            )
            val layoutParams = FrameLayout.LayoutParams(marginParams)
            view.randomNoteLL.layoutParams = layoutParams
            view.randomMainNote.layoutParams.height = height //note.noteHeight
            view.randomMainNote.layoutParams.width = width * 2//note.noteWidth * 2
// main note
            if (note.isTop()) {
                view.randomMainNote.setImageResource(R.drawable.main_note_top)
            } else {
                view.randomMainNote.setImageResource(R.drawable.main_note_bottom)
            }
            view.randomMainNote.layoutParams.height = height //note.noteHeight
            view.randomMainNote.layoutParams.width = width //note.noteWidth
// show sharp
            if (note.isSharp) {
                view.randomSharpSign.layoutParams.height = height //note.noteHeight
                view.randomSharpSign.layoutParams.width = width //note.noteWidth
            } else {
                view.randomSharpSign.visibility = View.INVISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteHolder {
        val inflatedView = parent.inflate(R.layout.note_item)
        return NoteHolder(inflatedView, baseHeight)
    }

    override fun onBindViewHolder(holder: NoteHolder, position: Int) {
        holder.bindView(sheet.getRandomNote(position))
    }

    override fun getItemCount(): Int {
        return sheet.getRandomNoteCount()
    }
}