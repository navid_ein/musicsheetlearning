package com.sai.learnsheetnote.repositories

import com.sai.learnsheetnote.App
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader


class FileReader {
    fun readFile(fileName: String): String? {
//            val iss: InputStream = App.context.resources.openRawResource(fileId)
//            val reader = BufferedReader(InputStreamReader(iss))
//            val sb = StringBuilder()
//            var line: String? = null
//            try {
//                while (reader.readLine().also { line = it } != null) {
//                    sb.append(line).append("\n")
//                }
//                reader.close()
//                return sb.toString()
//            } catch (e: IOException) {
//            }
//            return ""

        return this::class.java.getResource(fileName)?.readText()
    }
}