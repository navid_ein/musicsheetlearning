package com.navid.sheetmusic.timesignature

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import com.navid.sheetmusic.abstracts.ISymbolPainter
import com.navid.sheetmusic.abstracts.SectionNumber
import com.navid.sheetmusic.utils.MusicSymbols

class ThreeFourthMeterPainter(
    override var symbolColor: Int,
    override var symbolWidth: Int,
    override var symbolHeight: Int

) : ISymbolPainter {
    constructor(context: Context) : this(Color.BLACK, 0, 0) {
        this.context = context
    }

    var strokeWidth: Float = 15.0f
    fun symbolColor(symbolColor: Int) = apply { this.symbolColor = symbolColor }
    fun symbolWidth(symbolWidth: Int) = apply { this.symbolWidth = symbolWidth }
    fun symbolHeight(symbolHeight: Int) = apply { this.symbolHeight = symbolHeight }
    fun strokeWidth(strokeWidth: Float) = apply { this.strokeWidth = strokeWidth }

    lateinit var context: Context
    lateinit var resources: Resources

    override fun getXPosition(): Int {
        return 0
    }

    override fun getYPosition(): SectionNumber {
        return 9
    }

    override fun getBitmap(sectionHeight: Int): Bitmap {
        symbolHeight = 8 * sectionHeight
        return MusicSymbols.getThreeFourthMeter(symbolHeight, symbolColor, strokeWidth)

    }
}