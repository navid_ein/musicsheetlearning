package com.navid.sheetmusic.timesignature

import android.graphics.Bitmap
import com.navid.sheetmusic.abstracts.ISymbolPainter
import com.navid.sheetmusic.abstracts.ITimeSignature
import com.navid.sheetmusic.abstracts.SectionNumber

class ThreeFourthMeter(override var keyPainter: ISymbolPainter) : ITimeSignature {
    override fun getKey() {

    }

    override fun paintTimeSignature(sectionHeight: Int): Bitmap {
        return keyPainter.getBitmap(sectionHeight)
    }

    override fun symbolPosition(): SectionNumber {
        return keyPainter.getYPosition()
    }
}