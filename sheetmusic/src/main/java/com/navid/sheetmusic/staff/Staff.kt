package com.navid.sheetmusic.staff

import android.graphics.Canvas
import android.graphics.Paint
import com.navid.sheetmusic.abstracts.*
import com.navid.sheetmusic.clef.GClef


class Staff(
    override var linesNumber: Int,
    override var topLedgerLines: Int,
    override var bottomLedgerLines: Int,
    override var topMargin: Int,
    override var bottomMargin: Int,
    override var startMargin: Int,
    override var symbolSpace: Int


) : IStaff {
    constructor() : this(
        5, 0, 0,
        15, 15, 45, 5
    ) {

    }

    override lateinit var timeSignature: ITimeSignature
    override lateinit var keySignature: IKeySignature
    override lateinit var clef: IClef
    override lateinit var staffPainter: IStaffPainter

    companion object {
        const val StaffLinesNumber = 5
    }


    // Builder Functions
    fun timeSignature(timeSignature: ITimeSignature) = apply { this.timeSignature = timeSignature }
    fun keySignature(keySignature: IKeySignature) = apply { this.keySignature = keySignature }
    fun clef(clef: IClef) = apply { this.clef = clef }
    fun linesNumber(linesNumber: Int) = apply { this.linesNumber = linesNumber }
    fun topLedgerLines(topLedgerLines: Int) = apply { this.topLedgerLines = topLedgerLines }
    fun bottomLedgerLines(bottomLedgerLines: Int) =
        apply { this.bottomLedgerLines = bottomLedgerLines }

    fun staffPainter(staffPainter: IStaffPainter) = apply {
        staffPainter.staff = this
        staffPainter.bottomMargin = bottomMargin
        staffPainter.topMargin = topMargin
        staffPainter.startMargin = startMargin
        this.staffPainter = staffPainter
    }

    private val clefPaint: Paint by lazy {
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.strokeWidth = (staffPainter.pitchSize / 20).toFloat()
        paint.style = Paint.Style.STROKE
        paint.color = clef.clefPainter.symbolColor
        return@lazy paint
    }

    override fun paintStaff(canvas: Canvas) {
        // draw staff
        staffPainter.paintStaff(canvas)
        var xPosition = startMargin + symbolSpace

        // draw clef
        val clefBitmap = clef.paintClef(staffPainter.sectionHeight)
        val clefPosition = staffPainter.getYPositionOfSection(clef.clefPosition())
        canvas.drawBitmap(clefBitmap, xPosition.toFloat(), clefPosition.toFloat(), clefPaint)

        xPosition += clefBitmap.width + symbolSpace
        //draw key signature (scale)
        val scaleBitmap = keySignature.paintKeySignature(staffPainter.sectionHeight)
        val scalePosition = staffPainter.getYPositionOfSection(keySignature.keyPosition())
        canvas.drawBitmap(scaleBitmap, xPosition.toFloat(), scalePosition.toFloat(), clefPaint)

        xPosition += scaleBitmap.width + symbolSpace
        val meterBitmap = timeSignature.paintTimeSignature(staffPainter.sectionHeight)
        val meterPosition = staffPainter.getYPositionOfSection(timeSignature.symbolPosition())
        canvas.drawBitmap(meterBitmap, xPosition.toFloat(), meterPosition.toFloat(), clefPaint)


    }

}