package com.navid.sheetmusic.staff

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.navid.sheetmusic.abstracts.IStaff
import com.navid.sheetmusic.abstracts.IStaffPainter

class StaffPainter(
    override var staffColor: Int,
    override var staffHeight: Int,
    override var ledgerColor: Int,
    override var staffWidth: Int,
    override var topMargin: Int,
    override var bottomMargin: Int,
    override var startMargin: Int

) : IStaffPainter {
    constructor() : this(Color.BLACK, 0, Color.GRAY, 0, 0, 0, 0)

    override lateinit var staff: IStaff
    private val sections by lazy { staff.topLedgerLines + staff.bottomLedgerLines + staff.linesNumber + 2 }
    private val contentHeight by lazy { staffHeight - topMargin - bottomMargin }
    override val pitchSize by lazy { contentHeight / sections }
    override val sectionHeight by lazy { pitchSize / 2 }

    private val mainStaffPaint by lazy {
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.strokeWidth = (pitchSize / 15).toFloat()
        paint.style = Paint.Style.STROKE
        paint.color = staffColor
        return@lazy paint
    }

    private val badgerPaint: Paint by lazy {
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.strokeWidth = (pitchSize / 30).toFloat()
        paint.style = Paint.Style.STROKE
        paint.color = ledgerColor
        return@lazy paint
    }

    fun staffColor(staffColor: Int) = apply { this.staffColor = staffColor }
    fun staffHeight(staffHeight: Int) = apply { this.staffHeight = staffHeight }
    fun ledgerColor(ledgerColor: Int) = apply { this.ledgerColor = ledgerColor }
    fun staffWidth(staffWidth: Int) = apply { this.staffWidth = staffWidth }
//    fun topMargin(topMargin: Int) = apply { this.topMargin = topMargin }
//    fun bottomMargin(bottomMargin: Int) = apply { this.bottomMargin = bottomMargin }
//    fun startMargin(startMargin: Int) = apply { this.startMargin = startMargin }


    override fun paintStaff(canvas: Canvas) {
        drawStaffLines(canvas)
    }


    private fun drawStaffLines(canvas: Canvas) {
        var startY: Float = ((staff.topLedgerLines + 1 * pitchSize) + topMargin).toFloat()
        var startX: Float = startMargin.toFloat()

        for (i in 0 until staff.topLedgerLines) {
            canvas.drawLine(startX, startY, staffWidth.toFloat(), startY, badgerPaint)
            startY += pitchSize
        }

        for (i in 0 until staff.linesNumber) {
            canvas.drawLine(startX, startY, staffWidth.toFloat(), startY, mainStaffPaint)
            startY += pitchSize
        }

        for (i in 0 until staff.bottomLedgerLines) {
            canvas.drawLine(startX, startY, staffWidth.toFloat(), startY, badgerPaint)
            startY += pitchSize
        }

    }

    override fun getYPositionOfSection(num: Int): Int {
        val allTopSections = (staff.topLedgerLines + staff.linesNumber) * 2
        val realPos = (allTopSections - num) + 1

        return topMargin + (realPos * sectionHeight)

    }
}