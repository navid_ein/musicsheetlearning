package com.navid.sheetmusic.keysignature

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import com.navid.sheetmusic.abstracts.ISymbolPainter
import com.navid.sheetmusic.abstracts.SectionNumber
import com.navid.sheetmusic.utils.MusicSymbols


class DMajorScalePainterSmall(
    override var symbolColor: Int,
    override var symbolWidth: Int,
    override var symbolHeight: Int,
    var context: Context
) : ISymbolPainter {

    constructor(context: Context) : this(Color.BLACK, 0, 0, context)

    private var space = -15
    private val resources: Resources by lazy { context.resources }
    private val symbolPaint: Paint by lazy {
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = symbolColor
        return@lazy paint
    }

    fun symbolColor(symbolColor: Int) = apply { this.symbolColor = symbolColor }
    fun symbolWidth(symbolWidth: Int) = apply { this.symbolWidth = symbolWidth }
    fun symbolHeight(symbolHeight: Int) = apply { this.symbolHeight = symbolHeight }
    fun space(space: Int) = apply { this.space = space }

    override fun getXPosition(): Int {
        return 0
    }

    override fun getYPosition(): SectionNumber {
        return 11
    }

    override fun getBitmap(sectionHeight: Int): Bitmap {
        symbolHeight = sectionHeight * 10
        val sharpHeight = sectionHeight * 4
        val sharpWidth = sectionHeight * 2
//        val sharpBitmap = ImageResizer(resources, R.drawable.sharp).resizeBitmapHeight(sharpHeight)
//        val sharpBitmap = drawSharp(sharpWidth, sharpHeight)
        val sharpBitmap = MusicSymbols.getSharp(sharpWidth, sharpHeight, symbolColor)

        symbolWidth = (sharpBitmap.width * 2) + space

        val bitmap = Bitmap.createBitmap(
            symbolWidth,
            symbolHeight,
            Bitmap.Config.ARGB_8888
        )

        val firstSharpTopPosition = 0.toFloat()
        val firstSharpLeftPosition = 0.toFloat()
        val secondSharpTopPosition = (sectionHeight * 3).toFloat()
        val secondSharpLeftPosition = (sharpBitmap.width + space).toFloat()

        val canvas = Canvas(bitmap)
//        canvas.drawColor(Color.CYAN)
        canvas.drawBitmap(sharpBitmap, firstSharpLeftPosition, firstSharpTopPosition, symbolPaint)
        canvas.drawBitmap(sharpBitmap, secondSharpLeftPosition, secondSharpTopPosition, symbolPaint)

        return bitmap
    }


}