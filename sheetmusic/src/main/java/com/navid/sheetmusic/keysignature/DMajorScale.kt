package com.navid.sheetmusic.keysignature

import android.graphics.Bitmap
import com.navid.sheetmusic.abstracts.IKeySignature
import com.navid.sheetmusic.abstracts.ISymbolPainter
import com.navid.sheetmusic.abstracts.SectionNumber

class DMajorScale(override var keyPainter: ISymbolPainter) : IKeySignature {
    override fun getKey() {
    }

    override fun paintKeySignature(sectionHeight: Int): Bitmap {
        return keyPainter.getBitmap(sectionHeight)
    }

    override fun keyPosition(): SectionNumber {
        return keyPainter.getYPosition()
    }

}