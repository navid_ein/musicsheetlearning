package com.navid.sheetmusic.utils

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory

class ImageResizer(private var resources: Resources, private var imageSource: Int) {

    fun findScaleY(reqHeight: Int, height: Int): Double {
        return if (height == 0) (0).toDouble() else {
            height.toDouble() / reqHeight.toDouble()
        }
    }

    fun resizeBitmapHeight(reqHeight: Int): Bitmap {
        return BitmapFactory.Options().run {
            inJustDecodeBounds = true
            BitmapFactory.decodeResource(resources, imageSource, this)
            val scale = findScaleY(reqHeight, outHeight)
            inSampleSize = scale.toInt()
            inJustDecodeBounds = false
            val height = (outHeight / scale).toInt()
            val width = (outWidth / scale).toInt()
            val fb = BitmapFactory.decodeResource(resources, imageSource, this)
            Bitmap.createScaledBitmap(fb, width, height, true)

        }

    }


//    fun findScale(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
//        with(options) {
//            inJustDecodeBounds = true
//        }
//
//        BitmapFactory.decodeResource(resources, imageSource, options)
//        val imageHeight: Int = options.outHeight
//        val imageWidth: Int = options.outWidth
////        val imageType: String = options.outMimeType
//        return 5
//    }
//
//    fun loadImage() {
//
//        val options = BitmapFactory.Options().apply {
//            inJustDecodeBounds = true
//        }
//        val (height: Int, width: Int) = options.run { outHeight to outWidth }
//        options.run { options.outHeight }
//        BitmapFactory.decodeResource(resources, imageSource, options)
//        val imageHeight: Int = options.outHeight
//        val imageWidth: Int = options.outWidth
//        val imageType: String = options.outMimeType
//    }

}