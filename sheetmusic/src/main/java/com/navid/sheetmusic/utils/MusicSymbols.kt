package com.navid.sheetmusic.utils

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect

class MusicSymbols {

    companion object {
        fun getSharp(width: Int, height: Int, symbolColor: Int): Bitmap {
            val sharpBitmap = Bitmap.createBitmap(
                width,
                height,
                Bitmap.Config.ARGB_8888
            )
            val w3d = (width / 3).toFloat()
            val h6d = (height / 6).toFloat()
            val h3d = (height / 3).toFloat()

            val vlStroke = (width / 11).toFloat()
            val hlStroke = (width / 4).toFloat()

            val vl1sx = w3d - (vlStroke / 2)
            val vl1sy = h6d
            val vl1ex = vl1sx
            val vl1ey = height.toFloat()

            val vl2sx = w3d * 2 + (vlStroke / 2)
            val vl2sy = 0.toFloat()
            val vl2ex = vl2sx
            val vl2ey = height - h6d


            val hl1sx = 0.toFloat()
            val hl1sy = h3d + (hlStroke / 2)
            val hl1ex = width.toFloat()
            val hl1ey = h3d - (hlStroke / 2)


            val hl2sx = 0.toFloat()
            val hl2sy = (h3d * 2) + (hlStroke / 2)
            val hl2ex = width.toFloat()
            val hl2ey = (h3d * 2) - (hlStroke / 2)

            val canvas = Canvas(sharpBitmap)
            val vPainter = Paint(Paint.ANTI_ALIAS_FLAG).apply {
                strokeWidth = vlStroke
                color = symbolColor
            }
            val hPainter = Paint(Paint.ANTI_ALIAS_FLAG).apply {
                strokeWidth = hlStroke
                color = symbolColor
            }
            canvas.drawLine(vl1sx, vl1sy, vl1ex, vl1ey, vPainter)
            canvas.drawLine(vl2sx, vl2sy, vl2ex, vl2ey, vPainter)
            canvas.drawLine(hl1sx, hl1sy, hl1ex, hl1ey, hPainter)
            canvas.drawLine(hl2sx, hl2sy, hl2ex, hl2ey, hPainter)

            return sharpBitmap
        }

        fun getThreeFourthMeter(height: Int, symbolColor: Int, stroke: Float): Bitmap {


//            val hlStroke = (width / 4).toFloat()


            val textPainter = Paint(Paint.ANTI_ALIAS_FLAG).apply {
                strokeWidth = stroke
                color = symbolColor
            }

            findTextFontSizeForHeight("3", height / 2, textPainter)
            findTextFontSizeForHeight("4", height / 2, textPainter)
            val bounds = Rect()
            textPainter.getTextBounds("4", 0, "4".length, bounds)
            val calcHeight: Int = bounds.height()
            println("NL: reqHeight = ($height) -> $calcHeight")
            val width = bounds.width()
            println("NL: width = ($width) ")

            val three4thBitmap = Bitmap.createBitmap(
                width,
                height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(three4thBitmap)



            canvas.drawText("3", 0.toFloat(), calcHeight.toFloat(), textPainter)
            canvas.drawText("4", 0.toFloat(), height.toFloat(), textPainter)

            return three4thBitmap
        }

        private fun findTextFontSizeForHeight(text: String, reqHeight: Int, textPaint: Paint) {
            val baseFontSize = 12F
            val bounds = Rect()
            textPaint.textSize = baseFontSize
            textPaint.getTextBounds(text, 0, text.length, bounds)
            val height: Int = bounds.height()
            textPaint.textSize = (reqHeight * baseFontSize) / height


//            var sizeSmaller = true
//            var fontSize = 1.0F
//            var smallHeight = 0
//            var bigHeight = 0
//            var smallFontSize = 0F
//            var bigFontSize = 0F
//
//            while (sizeSmaller) {
//                val bounds = Rect()
//                textPaint.textSize = fontSize
//                textPaint.getTextBounds(text, 0, text.length, bounds)
//                val height: Int = bounds.height()
//                if (height > reqHeight) {
//                    bigHeight = height
//                    bigFontSize = fontSize
//                    sizeSmaller = false
//                } else {
//                    smallHeight = height
//                    smallFontSize = fontSize
//                    fontSize += 10
//                }
//            }
//fontSize =
//            return fontSize
        }
    }


}