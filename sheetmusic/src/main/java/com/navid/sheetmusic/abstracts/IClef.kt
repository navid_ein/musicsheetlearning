package com.navid.sheetmusic.abstracts

import android.graphics.Bitmap

interface IClef {
    var clefPainter: ISymbolPainter
    fun getKey()
    fun paintClef(sectionHeight: Int): Bitmap
    fun clefPosition(): SectionNumber
}