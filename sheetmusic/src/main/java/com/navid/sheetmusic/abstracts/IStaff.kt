package com.navid.sheetmusic.abstracts

import android.graphics.Canvas

interface IStaff {
    var staffPainter: IStaffPainter
    var topLedgerLines: Int
    var bottomLedgerLines: Int
    var linesNumber: Int
    var topMargin: Int
    var bottomMargin: Int
    var startMargin: Int
    var symbolSpace: Int

    var clef: IClef
    var keySignature: IKeySignature
    var timeSignature: ITimeSignature

    fun paintStaff(canvas: Canvas)


//    operator fun get(i: Int): Int
}