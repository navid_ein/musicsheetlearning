package com.navid.sheetmusic.abstracts

interface ISheet {
    var notesOnSheet: Int
    var noteHeight: Int
    fun getNotePosition(id: Int)
    fun getKeyImage()
}