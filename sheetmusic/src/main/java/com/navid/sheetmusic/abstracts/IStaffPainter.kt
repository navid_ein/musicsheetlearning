package com.navid.sheetmusic.abstracts

import android.graphics.Canvas

interface IStaffPainter {
    var staffColor: Int
    var ledgerColor: Int
    var staffWidth: Int
    var staffHeight: Int
    val pitchSize: Int
    val sectionHeight: Int
    var staff: IStaff
    var topMargin: Int
    var bottomMargin: Int
    var startMargin: Int

    fun paintStaff(canvas: Canvas)
    fun getYPositionOfSection(num: Int): Int
}