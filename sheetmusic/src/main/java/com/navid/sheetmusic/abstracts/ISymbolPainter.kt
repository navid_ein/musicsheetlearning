package com.navid.sheetmusic.abstracts

import android.graphics.Bitmap

interface ISymbolPainter {

    var symbolColor: Int
    var symbolWidth: Int
    var symbolHeight: Int

    fun getXPosition(): Int
    fun getYPosition(): SectionNumber
    fun getBitmap(sectionHeight: Int): Bitmap

}