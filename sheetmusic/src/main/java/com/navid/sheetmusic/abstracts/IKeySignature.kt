package com.navid.sheetmusic.abstracts

import android.graphics.Bitmap

interface IKeySignature {
    var keyPainter: ISymbolPainter
    fun getKey()
    fun paintKeySignature(sectionHeight: Int): Bitmap
    fun keyPosition(): SectionNumber

}