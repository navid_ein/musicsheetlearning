package com.navid.sheetmusic.abstracts

import android.graphics.Bitmap

interface ITimeSignature {
    var keyPainter: ISymbolPainter
    fun getKey()
    fun paintTimeSignature(sectionHeight: Int): Bitmap
    fun symbolPosition(): SectionNumber
}