package com.navid.sheetmusic.clef

import android.graphics.Bitmap
import com.navid.sheetmusic.abstracts.IClef
import com.navid.sheetmusic.abstracts.ISymbolPainter
import com.navid.sheetmusic.abstracts.SectionNumber

class GClef() : IClef {

    override lateinit var clefPainter: ISymbolPainter
    fun clefPainter(clefPainter: ISymbolPainter) = apply { this.clefPainter = clefPainter }

    override fun getKey() {
        TODO("Not yet implemented")
    }

    override fun clefPosition(): SectionNumber {
        return clefPainter.getYPosition()
    }

    override fun paintClef(sectionHeight: Int): Bitmap {
        return clefPainter.getBitmap(sectionHeight)
    }
}