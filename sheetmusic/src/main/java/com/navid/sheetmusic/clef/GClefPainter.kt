package com.navid.sheetmusic.clef

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import com.navid.sheetmusic.R
import com.navid.sheetmusic.abstracts.ISymbolPainter
import com.navid.sheetmusic.abstracts.SectionNumber
import com.navid.sheetmusic.utils.ImageResizer


class GClefPainter(
    override var symbolColor: Int,
    override var symbolWidth: Int,
    override var symbolHeight: Int,
) : ISymbolPainter {

    constructor() : this(Color.BLACK, 0, 0)

    lateinit var context: Context

    //    private val mPaint: Paint
    private val resources: Resources by lazy { context.resources }
    private val clefPaint: Paint by lazy {
        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = symbolColor
        return@lazy paint
    }


    override fun getBitmap(sectionHeight: Int): Bitmap {
        symbolHeight = sectionHeight * 13
        return ImageResizer(resources, R.drawable.gclef).resizeBitmapHeight(symbolHeight)
    }

    override fun getXPosition(): Int {
        return 0
    }

    override fun getYPosition(): SectionNumber {
        return 11
    }


    fun clefColor(clefColor: Int) = apply { this.symbolColor = clefColor }
    fun clefWidth(clefWidth: Int) = apply { this.symbolWidth = clefWidth }
    fun clefHeight(clefHeight: Int) = apply { this.symbolHeight = clefHeight }
    fun context(context: Context) = apply { this.context = context }

}