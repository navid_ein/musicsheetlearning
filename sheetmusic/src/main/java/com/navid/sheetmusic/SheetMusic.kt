package com.navid.sheetmusic

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.navid.sheetmusic.abstracts.IStaff
import com.navid.sheetmusic.staff.Staff

class SheetMusic : View {

    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, staff: IStaff) : super(context) {

    }

    constructor (context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor (context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )


    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, staff: IStaff) : this(
        context,
        attrs,
        defStyleAttr
    ) {

    }

    private var sheetHeight: Int = 0
    private var sheetWidth: Int = 0

    lateinit var staff: IStaff

    data class Builder(
        var context: Context,
        var attrs: AttributeSet,
        var defStyleAttr: Int,
        var staff: IStaff
    ) {
        fun context(context: Context) = apply {
            this.context = context

        }

        fun attrs(attrs: AttributeSet) = apply { this.attrs = attrs }
        fun defStyleAttr(defStyleAttr: Int) = apply { this.defStyleAttr = defStyleAttr }
        fun staff(staff: Staff) = apply { this.staff = staff }
        fun build() = SheetMusic(context, staff)

    }


    override fun onDraw(canvas: Canvas?) {

        super.onDraw(canvas)
        if (canvas != null) {
            staff.paintStaff(canvas)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        sheetWidth = w
        sheetHeight = h
        setMeasuredDimension(sheetWidth, sheetHeight)

        setMeasuredDimensionStaffs()
    }


    private fun setMeasuredDimensionStaffs() {
        staff.staffPainter.staffHeight = sheetHeight
        staff.staffPainter.staffWidth = sheetWidth

    }
}