package com.navid.sheetmusic

import org.junit.Test
import com.google.common.truth.Truth.assertWithMessage
import com.navid.sheetmusic.staff.Staff
import com.navid.sheetmusic.staff.StaffPainter

class TestStaffPainter {
    private val topBadgerLines = 3
    private val bottomBadgerLines = 1
    private val staffHeight = 300
    private var sut: StaffPainter = StaffPainter()
        .staffHeight(staffHeight)

    private val staff: Staff =
        Staff()
            .staffPainter(sut)
            .topLedgerLines(topBadgerLines)
            .bottomLedgerLines(bottomBadgerLines)


    @Test
    fun testStaffSize() {

        var pitchSize = sut.pitchSize
        assertWithMessage("pitch Size is not correct").that(pitchSize).isEqualTo(24)
    }
    @Test
    fun testStaffSectionPositionTopLedger() {
        val pos = sut.getYPositionOfSection(16)
        assertWithMessage("y position of last top ledger is not correct").that(pos).isEqualTo(27)
    }
    @Test
    fun testStaffSectionPositionFirstTopLedger() {
        val pos = sut.getYPositionOfSection(12)
        assertWithMessage("y position of first top ledger is not correct").that(pos).isEqualTo(75)
    }
    @Test
    fun testStaffSectionPositionZero() {
        val pos = sut.getYPositionOfSection(0)
        assertWithMessage("y position of main line is not correct").that(pos).isEqualTo(219)
    }

//    @Test
//    fun testStaffSectionPositionIsNullBottom() {
//        val pos = sut.getYPositionOfSection(-3)
//        assertWithMessage("y position is not null").that(pos).isNull()
//    }

//    @Test
//    fun testStaffSectionPositionIsNullTop() {
//        val pos = sut.getYPositionOfSection(7)
//        assertWithMessage("y position is not null").that(pos).isNull()
//    }

//    @Test
//    fun testStaffSectionPositionNullBottom() {
//        val pos = sut.getYPositionOfSection(3)
//        assertWithMessage("y position is not null").that(pos).isEqualTo(30)
//    }

    @Test
    fun testStaffSectionPositionOfBottomLedger() {
        val pos = sut.getYPositionOfSection(-2)
        assertWithMessage("y position of bottom ledger is not correct").that(pos).isEqualTo(243)
    }



}