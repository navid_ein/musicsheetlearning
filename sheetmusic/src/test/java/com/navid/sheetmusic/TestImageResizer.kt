package com.navid.sheetmusic

import android.app.Activity
import android.app.Application
import android.content.res.Resources
import android.graphics.BitmapFactory
import androidx.test.core.app.ApplicationProvider
import com.navid.sheetmusic.utils.ImageResizer
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import com.google.common.truth.Truth.assertWithMessage

@RunWith(RobolectricTestRunner::class)
class TestImageResizer {
    var resources: Resources = ApplicationProvider.getApplicationContext<Application>().resources
    var sut = ImageResizer(resources, R.drawable.gclef)

    @Test
    fun testYScaleSmaller() {
        val scale = sut.findScaleY(2000, 5000)
        assertWithMessage("Y scale smaller is not correct").that(scale).isEqualTo(0.4)
    }

    @Test
    fun testYScaleLarger() {
        val scale = sut.findScaleY(7000, 5000)
        assertWithMessage("Y scale larger is not correct").that(scale).isEqualTo(1.4)
    }

    @Test
    fun testYScaleFail() {
        val scale = sut.findScaleY(0, 0)
        assertWithMessage("Y scale fail is not correct").that(scale).isEqualTo(0)
    }


    @Test
    fun testBitmapScaleY() {
        val newBitmap = sut.resizeBitmapHeight(200)
        assertWithMessage("new bitmap size is correct").that(newBitmap.height).isEqualTo(200)
    }

}