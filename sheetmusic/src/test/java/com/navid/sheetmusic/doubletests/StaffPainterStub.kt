package com.navid.sheetmusic.doubletests

import android.graphics.Canvas
import android.graphics.Color
import com.navid.sheetmusic.abstracts.IStaff
import com.navid.sheetmusic.abstracts.IStaffPainter
import com.navid.sheetmusic.abstracts.SectionNumber

class StaffPainterStub(
    override var staffColor: Int = Color.BLACK,
    override var ledgerColor: Int = Color.CYAN,
    override var staffWidth: Int = 200,
    override var staffHeight: Int = 400,
    override val pitchSize: Int = 20,
    override val sectionHeight: Int,
    override var topMargin: Int,
    override var bottomMargin: Int,
    override var startMargin: Int
) : IStaffPainter {
    override var staff: IStaff
        get() = TODO("Not yet implemented")
        set(value) {}

    override fun paintStaff(canvas: Canvas) {
    }

    override fun getYPositionOfSection(num: Int): SectionNumber {
        return 0
    }

}